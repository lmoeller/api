syntax = "proto3";

package eventmanager.frontend.feedback;
option go_package = "gitlab.ethz.ch/lmoeller/api/eventmanager/frontend/feedback";

import "eventmanager/frontend/helper/helper.proto";
import "eventmanager/frontend/option/option.proto";
import "eventmanager/frontend/participant/participant.proto";
import "eventmanager/frontend/person/person.proto";



// The feedback service allows for managing all event feedbacks provided by event-participants.
service FeedbackService {
    // Gets single feedback instance
    rpc GetFeedback(GetFeedbackRequest) returns (Feedback) {}
    // Creates a feedback for a user and an event
    rpc CreateFeedback(CreateFeedbackRequest) returns (Feedback) {}
    // Updates the content of a feedback
    rpc UpdateFeedback(UpdateFeedbackRequest) returns (Feedback) {}
    // Gets a list of feedbacks
    rpc ListFeedbacks(ListFeedbacksRequest) returns (ListFeedbacksResponse) {}
    // Marks a given feedback as being offensive
    rpc MarkFeedbackOffensive(MarkFeedbackOffensiveRequest) returns (Feedback) {}


    // Gets single feedback template instance given its ID or the event's ID
    rpc GetFeedbackTemplate(GetFeedbackTemplateRequest) returns (FeedbackTemplate) {}
    // Creates a feedback template
    rpc CreateFeedbackTemplate(CreateFeedbackTemplateRequest) returns (FeedbackTemplate) {}
    // Updates a feedback template
    rpc UpdateFeedbackTemplate(UpdateFeedbackTemplateRequest) returns (FeedbackTemplate) {}
    // Gets a list of feedback templates
    rpc ListFeedbackTemplates(ListFeedbackTemplatesRequest) returns (ListFeedbackTemplatesResponse) {}
    // Resets a template back to default version
    rpc ResetFeedbackTemplateToDefault(ResetFeedbackTemplateToDefaultRequest) returns (FeedbackTemplate) {}
}

message GetFeedbackRequest{
    string feedback_id = 1;
}

message CreateFeedbackRequest{
    Feedback feedback = 1;
}

message UpdateFeedbackRequest{
    Feedback feedback = 1;
}

message ListFeedbacksRequest{
    int32 page_size = 1;
    string page_token = 2;

    // Filters
    string event_id = 3;
    eventmanager.frontend.person.Person person = 4;
}

message ListFeedbacksResponse{
    repeated Feedback feedback = 1;
    string next_page_token = 2;
}

message MarkFeedbackOffensiveRequest {
    string feedback_id = 1;
}

message GetFeedbackTemplateRequest{
    oneof id {
        string feedback_template_id = 1;
        string event_id = 2;
        bool default = 3;
    }
}

message CreateFeedbackTemplateRequest{
    FeedbackTemplate feedback = 1;
}

message UpdateFeedbackTemplateRequest{
    FeedbackTemplate feedback = 1;
    bool default = 2; // true iff updating the admin default template is requested
}

message ListFeedbackTemplatesRequest{
    int32 page_size = 1;
    string page_token = 2;
}

message ListFeedbackTemplatesResponse{
    repeated FeedbackTemplate feedback = 1;
    string next_page_token = 2;
}

message ResetFeedbackTemplateToDefaultRequest {
    string id = 1;
}


// A feedback for events. Each participant is allowed to give feedback.
message Feedback {
    string id = 1;

    // Fields in the feedback form
    repeated eventmanager.frontend.option.OptionChoice choices = 2;

    oneof origin {
        // Participation for which the feedback is given
        eventmanager.frontend.participant.Participation participation = 3;

        // if no participation is given, then the feedback is given by a helper
        eventmanager.frontend.helper.Helper helper = 4;

        // Feedback also needs to be possible by token only
        string helper_token = 6;
        string participation_token = 7;
    }

    string feedback_template_id = 5;

    // Offensive feedback is hidden
    bool offensive = 8;
}

message FeedbackTemplate {
    string id = 1;

    string event_id = 2;

    repeated eventmanager.frontend.option.Option questions = 3;

    bool disabled = 4;
}
