// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package person

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// PersonServiceClient is the client API for PersonService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type PersonServiceClient interface {
	// Returns the currently logged in user or an error if not logged in
	GetLoggedinPerson(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*Person, error)
	// Returns true iff the person logged in is a board member
	GetPrivileged(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*PrivilegedResponse, error)
	// Lists persons matching filter
	ListPersons(ctx context.Context, in *ListPersonsRequest, opts ...grpc.CallOption) (*ListPersonsResponse, error)
}

type personServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPersonServiceClient(cc grpc.ClientConnInterface) PersonServiceClient {
	return &personServiceClient{cc}
}

func (c *personServiceClient) GetLoggedinPerson(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*Person, error) {
	out := new(Person)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.person.PersonService/GetLoggedinPerson", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *personServiceClient) GetPrivileged(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*PrivilegedResponse, error) {
	out := new(PrivilegedResponse)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.person.PersonService/GetPrivileged", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *personServiceClient) ListPersons(ctx context.Context, in *ListPersonsRequest, opts ...grpc.CallOption) (*ListPersonsResponse, error) {
	out := new(ListPersonsResponse)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.person.PersonService/ListPersons", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PersonServiceServer is the server API for PersonService service.
// All implementations must embed UnimplementedPersonServiceServer
// for forward compatibility
type PersonServiceServer interface {
	// Returns the currently logged in user or an error if not logged in
	GetLoggedinPerson(context.Context, *empty.Empty) (*Person, error)
	// Returns true iff the person logged in is a board member
	GetPrivileged(context.Context, *empty.Empty) (*PrivilegedResponse, error)
	// Lists persons matching filter
	ListPersons(context.Context, *ListPersonsRequest) (*ListPersonsResponse, error)
	mustEmbedUnimplementedPersonServiceServer()
}

// UnimplementedPersonServiceServer must be embedded to have forward compatible implementations.
type UnimplementedPersonServiceServer struct {
}

func (UnimplementedPersonServiceServer) GetLoggedinPerson(context.Context, *empty.Empty) (*Person, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetLoggedinPerson not implemented")
}
func (UnimplementedPersonServiceServer) GetPrivileged(context.Context, *empty.Empty) (*PrivilegedResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPrivileged not implemented")
}
func (UnimplementedPersonServiceServer) ListPersons(context.Context, *ListPersonsRequest) (*ListPersonsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListPersons not implemented")
}
func (UnimplementedPersonServiceServer) mustEmbedUnimplementedPersonServiceServer() {}

// UnsafePersonServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to PersonServiceServer will
// result in compilation errors.
type UnsafePersonServiceServer interface {
	mustEmbedUnimplementedPersonServiceServer()
}

func RegisterPersonServiceServer(s grpc.ServiceRegistrar, srv PersonServiceServer) {
	s.RegisterService(&_PersonService_serviceDesc, srv)
}

func _PersonService_GetLoggedinPerson_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PersonServiceServer).GetLoggedinPerson(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.person.PersonService/GetLoggedinPerson",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PersonServiceServer).GetLoggedinPerson(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _PersonService_GetPrivileged_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PersonServiceServer).GetPrivileged(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.person.PersonService/GetPrivileged",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PersonServiceServer).GetPrivileged(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _PersonService_ListPersons_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListPersonsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PersonServiceServer).ListPersons(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.person.PersonService/ListPersons",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PersonServiceServer).ListPersons(ctx, req.(*ListPersonsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _PersonService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "eventmanager.frontend.person.PersonService",
	HandlerType: (*PersonServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetLoggedinPerson",
			Handler:    _PersonService_GetLoggedinPerson_Handler,
		},
		{
			MethodName: "GetPrivileged",
			Handler:    _PersonService_GetPrivileged_Handler,
		},
		{
			MethodName: "ListPersons",
			Handler:    _PersonService_ListPersons_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "eventmanager/frontend/person/person.proto",
}
