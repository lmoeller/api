// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package helper

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// HelperServiceClient is the client API for HelperService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type HelperServiceClient interface {
	// Gets the helper's intent of the provided user
	GetHelpersIntent(ctx context.Context, in *GetHelpersIntentRequest, opts ...grpc.CallOption) (*HelpersIntent, error)
	// Updates the helper's intent of the provided user
	UpdateHelpersIntent(ctx context.Context, in *UpdateHelpersIntentRequest, opts ...grpc.CallOption) (*HelpersIntent, error)
	// Creates the helperslot
	CreateHelperSlot(ctx context.Context, in *CreateHelperSlotRequest, opts ...grpc.CallOption) (*HelperSlot, error)
	// Gets the helperslot
	GetHelperSlot(ctx context.Context, in *GetHelperSlotRequest, opts ...grpc.CallOption) (*HelperSlot, error)
	// Deletes the helperslot and all of the signed up helpers
	DeleteHelperSlot(ctx context.Context, in *DeleteHelperSlotRequest, opts ...grpc.CallOption) (*empty.Empty, error)
	// Updates the helperslot
	UpdateHelperSlot(ctx context.Context, in *UpdateHelperSlotRequest, opts ...grpc.CallOption) (*HelperSlot, error)
	// Lists open helper slots for the current user
	ListOpenHelperSlots(ctx context.Context, in *ListOpenHelperSlotsRequest, opts ...grpc.CallOption) (*ListOpenHelperSlotsResponse, error)
	// Lists helper slots the provided user has "booked"
	ListBookedHelperSlots(ctx context.Context, in *ListBookedHelperSlotsRequest, opts ...grpc.CallOption) (*ListBookedHelperSlotsResponse, error)
	// Lists helper slots for provided event
	ListHelperSlotsForEvent(ctx context.Context, in *ListHelperSlotsForEventRequest, opts ...grpc.CallOption) (*ListHelperSlotsForEventResponse, error)
	// Books a helper slot for the provided user.
	CreateHelper(ctx context.Context, in *CreateHelperRequest, opts ...grpc.CallOption) (*Helper, error)
	// Retuns the helper slot booking.
	GetHelper(ctx context.Context, in *GetHelperRequest, opts ...grpc.CallOption) (*Helper, error)
	// Updates the helper slot booking.
	UpdateHelper(ctx context.Context, in *UpdateHelperRequest, opts ...grpc.CallOption) (*Helper, error)
	// Deletes the helper slot booking.
	DeleteHelper(ctx context.Context, in *DeleteHelperRequest, opts ...grpc.CallOption) (*empty.Empty, error)
	// Cancels the signup for the helper slot
	CancelHelper(ctx context.Context, in *CancelHelperRequest, opts ...grpc.CallOption) (*Helper, error)
	// Resets the helper signup to SIGNED_UP if possible
	ResetHelperStatus(ctx context.Context, in *ResetHelperStatusRequest, opts ...grpc.CallOption) (*Helper, error)
	// Lists helpers for given filter.
	ListHelpers(ctx context.Context, in *ListHelpersRequest, opts ...grpc.CallOption) (*ListHelpersResponse, error)
	// Sends out a message to all helpers of the event. Helpers joining after
	// the message is sent out will receive the message once they join.
	CreateHelperMessage(ctx context.Context, in *CreateHelperMessageRequest, opts ...grpc.CallOption) (*HelperMessage, error)
}

type helperServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewHelperServiceClient(cc grpc.ClientConnInterface) HelperServiceClient {
	return &helperServiceClient{cc}
}

func (c *helperServiceClient) GetHelpersIntent(ctx context.Context, in *GetHelpersIntentRequest, opts ...grpc.CallOption) (*HelpersIntent, error) {
	out := new(HelpersIntent)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/GetHelpersIntent", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) UpdateHelpersIntent(ctx context.Context, in *UpdateHelpersIntentRequest, opts ...grpc.CallOption) (*HelpersIntent, error) {
	out := new(HelpersIntent)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/UpdateHelpersIntent", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) CreateHelperSlot(ctx context.Context, in *CreateHelperSlotRequest, opts ...grpc.CallOption) (*HelperSlot, error) {
	out := new(HelperSlot)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/CreateHelperSlot", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) GetHelperSlot(ctx context.Context, in *GetHelperSlotRequest, opts ...grpc.CallOption) (*HelperSlot, error) {
	out := new(HelperSlot)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/GetHelperSlot", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) DeleteHelperSlot(ctx context.Context, in *DeleteHelperSlotRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/DeleteHelperSlot", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) UpdateHelperSlot(ctx context.Context, in *UpdateHelperSlotRequest, opts ...grpc.CallOption) (*HelperSlot, error) {
	out := new(HelperSlot)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/UpdateHelperSlot", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) ListOpenHelperSlots(ctx context.Context, in *ListOpenHelperSlotsRequest, opts ...grpc.CallOption) (*ListOpenHelperSlotsResponse, error) {
	out := new(ListOpenHelperSlotsResponse)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/ListOpenHelperSlots", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) ListBookedHelperSlots(ctx context.Context, in *ListBookedHelperSlotsRequest, opts ...grpc.CallOption) (*ListBookedHelperSlotsResponse, error) {
	out := new(ListBookedHelperSlotsResponse)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/ListBookedHelperSlots", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) ListHelperSlotsForEvent(ctx context.Context, in *ListHelperSlotsForEventRequest, opts ...grpc.CallOption) (*ListHelperSlotsForEventResponse, error) {
	out := new(ListHelperSlotsForEventResponse)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/ListHelperSlotsForEvent", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) CreateHelper(ctx context.Context, in *CreateHelperRequest, opts ...grpc.CallOption) (*Helper, error) {
	out := new(Helper)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/CreateHelper", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) GetHelper(ctx context.Context, in *GetHelperRequest, opts ...grpc.CallOption) (*Helper, error) {
	out := new(Helper)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/GetHelper", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) UpdateHelper(ctx context.Context, in *UpdateHelperRequest, opts ...grpc.CallOption) (*Helper, error) {
	out := new(Helper)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/UpdateHelper", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) DeleteHelper(ctx context.Context, in *DeleteHelperRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/DeleteHelper", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) CancelHelper(ctx context.Context, in *CancelHelperRequest, opts ...grpc.CallOption) (*Helper, error) {
	out := new(Helper)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/CancelHelper", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) ResetHelperStatus(ctx context.Context, in *ResetHelperStatusRequest, opts ...grpc.CallOption) (*Helper, error) {
	out := new(Helper)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/ResetHelperStatus", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) ListHelpers(ctx context.Context, in *ListHelpersRequest, opts ...grpc.CallOption) (*ListHelpersResponse, error) {
	out := new(ListHelpersResponse)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/ListHelpers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helperServiceClient) CreateHelperMessage(ctx context.Context, in *CreateHelperMessageRequest, opts ...grpc.CallOption) (*HelperMessage, error) {
	out := new(HelperMessage)
	err := c.cc.Invoke(ctx, "/eventmanager.frontend.helper.HelperService/CreateHelperMessage", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// HelperServiceServer is the server API for HelperService service.
// All implementations must embed UnimplementedHelperServiceServer
// for forward compatibility
type HelperServiceServer interface {
	// Gets the helper's intent of the provided user
	GetHelpersIntent(context.Context, *GetHelpersIntentRequest) (*HelpersIntent, error)
	// Updates the helper's intent of the provided user
	UpdateHelpersIntent(context.Context, *UpdateHelpersIntentRequest) (*HelpersIntent, error)
	// Creates the helperslot
	CreateHelperSlot(context.Context, *CreateHelperSlotRequest) (*HelperSlot, error)
	// Gets the helperslot
	GetHelperSlot(context.Context, *GetHelperSlotRequest) (*HelperSlot, error)
	// Deletes the helperslot and all of the signed up helpers
	DeleteHelperSlot(context.Context, *DeleteHelperSlotRequest) (*empty.Empty, error)
	// Updates the helperslot
	UpdateHelperSlot(context.Context, *UpdateHelperSlotRequest) (*HelperSlot, error)
	// Lists open helper slots for the current user
	ListOpenHelperSlots(context.Context, *ListOpenHelperSlotsRequest) (*ListOpenHelperSlotsResponse, error)
	// Lists helper slots the provided user has "booked"
	ListBookedHelperSlots(context.Context, *ListBookedHelperSlotsRequest) (*ListBookedHelperSlotsResponse, error)
	// Lists helper slots for provided event
	ListHelperSlotsForEvent(context.Context, *ListHelperSlotsForEventRequest) (*ListHelperSlotsForEventResponse, error)
	// Books a helper slot for the provided user.
	CreateHelper(context.Context, *CreateHelperRequest) (*Helper, error)
	// Retuns the helper slot booking.
	GetHelper(context.Context, *GetHelperRequest) (*Helper, error)
	// Updates the helper slot booking.
	UpdateHelper(context.Context, *UpdateHelperRequest) (*Helper, error)
	// Deletes the helper slot booking.
	DeleteHelper(context.Context, *DeleteHelperRequest) (*empty.Empty, error)
	// Cancels the signup for the helper slot
	CancelHelper(context.Context, *CancelHelperRequest) (*Helper, error)
	// Resets the helper signup to SIGNED_UP if possible
	ResetHelperStatus(context.Context, *ResetHelperStatusRequest) (*Helper, error)
	// Lists helpers for given filter.
	ListHelpers(context.Context, *ListHelpersRequest) (*ListHelpersResponse, error)
	// Sends out a message to all helpers of the event. Helpers joining after
	// the message is sent out will receive the message once they join.
	CreateHelperMessage(context.Context, *CreateHelperMessageRequest) (*HelperMessage, error)
	mustEmbedUnimplementedHelperServiceServer()
}

// UnimplementedHelperServiceServer must be embedded to have forward compatible implementations.
type UnimplementedHelperServiceServer struct {
}

func (UnimplementedHelperServiceServer) GetHelpersIntent(context.Context, *GetHelpersIntentRequest) (*HelpersIntent, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetHelpersIntent not implemented")
}
func (UnimplementedHelperServiceServer) UpdateHelpersIntent(context.Context, *UpdateHelpersIntentRequest) (*HelpersIntent, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateHelpersIntent not implemented")
}
func (UnimplementedHelperServiceServer) CreateHelperSlot(context.Context, *CreateHelperSlotRequest) (*HelperSlot, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateHelperSlot not implemented")
}
func (UnimplementedHelperServiceServer) GetHelperSlot(context.Context, *GetHelperSlotRequest) (*HelperSlot, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetHelperSlot not implemented")
}
func (UnimplementedHelperServiceServer) DeleteHelperSlot(context.Context, *DeleteHelperSlotRequest) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteHelperSlot not implemented")
}
func (UnimplementedHelperServiceServer) UpdateHelperSlot(context.Context, *UpdateHelperSlotRequest) (*HelperSlot, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateHelperSlot not implemented")
}
func (UnimplementedHelperServiceServer) ListOpenHelperSlots(context.Context, *ListOpenHelperSlotsRequest) (*ListOpenHelperSlotsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListOpenHelperSlots not implemented")
}
func (UnimplementedHelperServiceServer) ListBookedHelperSlots(context.Context, *ListBookedHelperSlotsRequest) (*ListBookedHelperSlotsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListBookedHelperSlots not implemented")
}
func (UnimplementedHelperServiceServer) ListHelperSlotsForEvent(context.Context, *ListHelperSlotsForEventRequest) (*ListHelperSlotsForEventResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListHelperSlotsForEvent not implemented")
}
func (UnimplementedHelperServiceServer) CreateHelper(context.Context, *CreateHelperRequest) (*Helper, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateHelper not implemented")
}
func (UnimplementedHelperServiceServer) GetHelper(context.Context, *GetHelperRequest) (*Helper, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetHelper not implemented")
}
func (UnimplementedHelperServiceServer) UpdateHelper(context.Context, *UpdateHelperRequest) (*Helper, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateHelper not implemented")
}
func (UnimplementedHelperServiceServer) DeleteHelper(context.Context, *DeleteHelperRequest) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteHelper not implemented")
}
func (UnimplementedHelperServiceServer) CancelHelper(context.Context, *CancelHelperRequest) (*Helper, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CancelHelper not implemented")
}
func (UnimplementedHelperServiceServer) ResetHelperStatus(context.Context, *ResetHelperStatusRequest) (*Helper, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ResetHelperStatus not implemented")
}
func (UnimplementedHelperServiceServer) ListHelpers(context.Context, *ListHelpersRequest) (*ListHelpersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListHelpers not implemented")
}
func (UnimplementedHelperServiceServer) CreateHelperMessage(context.Context, *CreateHelperMessageRequest) (*HelperMessage, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateHelperMessage not implemented")
}
func (UnimplementedHelperServiceServer) mustEmbedUnimplementedHelperServiceServer() {}

// UnsafeHelperServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to HelperServiceServer will
// result in compilation errors.
type UnsafeHelperServiceServer interface {
	mustEmbedUnimplementedHelperServiceServer()
}

func RegisterHelperServiceServer(s grpc.ServiceRegistrar, srv HelperServiceServer) {
	s.RegisterService(&_HelperService_serviceDesc, srv)
}

func _HelperService_GetHelpersIntent_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetHelpersIntentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).GetHelpersIntent(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/GetHelpersIntent",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).GetHelpersIntent(ctx, req.(*GetHelpersIntentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_UpdateHelpersIntent_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateHelpersIntentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).UpdateHelpersIntent(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/UpdateHelpersIntent",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).UpdateHelpersIntent(ctx, req.(*UpdateHelpersIntentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_CreateHelperSlot_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateHelperSlotRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).CreateHelperSlot(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/CreateHelperSlot",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).CreateHelperSlot(ctx, req.(*CreateHelperSlotRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_GetHelperSlot_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetHelperSlotRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).GetHelperSlot(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/GetHelperSlot",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).GetHelperSlot(ctx, req.(*GetHelperSlotRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_DeleteHelperSlot_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteHelperSlotRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).DeleteHelperSlot(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/DeleteHelperSlot",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).DeleteHelperSlot(ctx, req.(*DeleteHelperSlotRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_UpdateHelperSlot_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateHelperSlotRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).UpdateHelperSlot(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/UpdateHelperSlot",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).UpdateHelperSlot(ctx, req.(*UpdateHelperSlotRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_ListOpenHelperSlots_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListOpenHelperSlotsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).ListOpenHelperSlots(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/ListOpenHelperSlots",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).ListOpenHelperSlots(ctx, req.(*ListOpenHelperSlotsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_ListBookedHelperSlots_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListBookedHelperSlotsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).ListBookedHelperSlots(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/ListBookedHelperSlots",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).ListBookedHelperSlots(ctx, req.(*ListBookedHelperSlotsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_ListHelperSlotsForEvent_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListHelperSlotsForEventRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).ListHelperSlotsForEvent(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/ListHelperSlotsForEvent",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).ListHelperSlotsForEvent(ctx, req.(*ListHelperSlotsForEventRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_CreateHelper_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateHelperRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).CreateHelper(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/CreateHelper",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).CreateHelper(ctx, req.(*CreateHelperRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_GetHelper_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetHelperRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).GetHelper(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/GetHelper",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).GetHelper(ctx, req.(*GetHelperRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_UpdateHelper_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateHelperRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).UpdateHelper(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/UpdateHelper",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).UpdateHelper(ctx, req.(*UpdateHelperRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_DeleteHelper_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteHelperRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).DeleteHelper(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/DeleteHelper",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).DeleteHelper(ctx, req.(*DeleteHelperRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_CancelHelper_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CancelHelperRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).CancelHelper(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/CancelHelper",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).CancelHelper(ctx, req.(*CancelHelperRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_ResetHelperStatus_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ResetHelperStatusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).ResetHelperStatus(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/ResetHelperStatus",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).ResetHelperStatus(ctx, req.(*ResetHelperStatusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_ListHelpers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListHelpersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).ListHelpers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/ListHelpers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).ListHelpers(ctx, req.(*ListHelpersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelperService_CreateHelperMessage_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateHelperMessageRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelperServiceServer).CreateHelperMessage(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/eventmanager.frontend.helper.HelperService/CreateHelperMessage",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelperServiceServer).CreateHelperMessage(ctx, req.(*CreateHelperMessageRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _HelperService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "eventmanager.frontend.helper.HelperService",
	HandlerType: (*HelperServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetHelpersIntent",
			Handler:    _HelperService_GetHelpersIntent_Handler,
		},
		{
			MethodName: "UpdateHelpersIntent",
			Handler:    _HelperService_UpdateHelpersIntent_Handler,
		},
		{
			MethodName: "CreateHelperSlot",
			Handler:    _HelperService_CreateHelperSlot_Handler,
		},
		{
			MethodName: "GetHelperSlot",
			Handler:    _HelperService_GetHelperSlot_Handler,
		},
		{
			MethodName: "DeleteHelperSlot",
			Handler:    _HelperService_DeleteHelperSlot_Handler,
		},
		{
			MethodName: "UpdateHelperSlot",
			Handler:    _HelperService_UpdateHelperSlot_Handler,
		},
		{
			MethodName: "ListOpenHelperSlots",
			Handler:    _HelperService_ListOpenHelperSlots_Handler,
		},
		{
			MethodName: "ListBookedHelperSlots",
			Handler:    _HelperService_ListBookedHelperSlots_Handler,
		},
		{
			MethodName: "ListHelperSlotsForEvent",
			Handler:    _HelperService_ListHelperSlotsForEvent_Handler,
		},
		{
			MethodName: "CreateHelper",
			Handler:    _HelperService_CreateHelper_Handler,
		},
		{
			MethodName: "GetHelper",
			Handler:    _HelperService_GetHelper_Handler,
		},
		{
			MethodName: "UpdateHelper",
			Handler:    _HelperService_UpdateHelper_Handler,
		},
		{
			MethodName: "DeleteHelper",
			Handler:    _HelperService_DeleteHelper_Handler,
		},
		{
			MethodName: "CancelHelper",
			Handler:    _HelperService_CancelHelper_Handler,
		},
		{
			MethodName: "ResetHelperStatus",
			Handler:    _HelperService_ResetHelperStatus_Handler,
		},
		{
			MethodName: "ListHelpers",
			Handler:    _HelperService_ListHelpers_Handler,
		},
		{
			MethodName: "CreateHelperMessage",
			Handler:    _HelperService_CreateHelperMessage_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "eventmanager/frontend/helper/helper.proto",
}
