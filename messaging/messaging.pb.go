// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.11.4
// source: message/messaging.proto

package messaging

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type MailRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The name to display when sending as sys user e.g. Admin. Due to technical
	// reasons it will always be the same sending address
	From    string   `protobuf:"bytes,1,opt,name=from,proto3" json:"from,omitempty"`
	ReplyTo string   `protobuf:"bytes,2,opt,name=reply_to,json=replyTo,proto3" json:"reply_to,omitempty"` // What address to reply to. e.g. "LUK <luk@vis.ethz.ch>"
	To      []string `protobuf:"bytes,3,rep,name=to,proto3" json:"to,omitempty"`                          // List of to. Format: "Admin <root@vis.ethz.ch>" - will result in error if not provided
	Cc      []string `protobuf:"bytes,4,rep,name=cc,proto3" json:"cc,omitempty"`                          //  List of cc. Format: "Admin <root@vis.ethz.ch>"
	Bcc     []string `protobuf:"bytes,5,rep,name=bcc,proto3" json:"bcc,omitempty"`                        // List of bcc.
	Subject string   `protobuf:"bytes,6,opt,name=subject,proto3" json:"subject,omitempty"`                // Subject of the message - will result in error if not provided
	Body    string   `protobuf:"bytes,7,opt,name=body,proto3" json:"body,omitempty"`                      // The content of the message - will result in error if not provided
}

func (x *MailRequest) Reset() {
	*x = MailRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_message_messaging_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MailRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MailRequest) ProtoMessage() {}

func (x *MailRequest) ProtoReflect() protoreflect.Message {
	mi := &file_message_messaging_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MailRequest.ProtoReflect.Descriptor instead.
func (*MailRequest) Descriptor() ([]byte, []int) {
	return file_message_messaging_proto_rawDescGZIP(), []int{0}
}

func (x *MailRequest) GetFrom() string {
	if x != nil {
		return x.From
	}
	return ""
}

func (x *MailRequest) GetReplyTo() string {
	if x != nil {
		return x.ReplyTo
	}
	return ""
}

func (x *MailRequest) GetTo() []string {
	if x != nil {
		return x.To
	}
	return nil
}

func (x *MailRequest) GetCc() []string {
	if x != nil {
		return x.Cc
	}
	return nil
}

func (x *MailRequest) GetBcc() []string {
	if x != nil {
		return x.Bcc
	}
	return nil
}

func (x *MailRequest) GetSubject() string {
	if x != nil {
		return x.Subject
	}
	return ""
}

func (x *MailRequest) GetBody() string {
	if x != nil {
		return x.Body
	}
	return ""
}

type MailResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *MailResponse) Reset() {
	*x = MailResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_message_messaging_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MailResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MailResponse) ProtoMessage() {}

func (x *MailResponse) ProtoReflect() protoreflect.Message {
	mi := &file_message_messaging_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MailResponse.ProtoReflect.Descriptor instead.
func (*MailResponse) Descriptor() ([]byte, []int) {
	return file_message_messaging_proto_rawDescGZIP(), []int{1}
}

type ImRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Recipient string `protobuf:"bytes,1,opt,name=recipient,proto3" json:"recipient,omitempty"` // The nethz of the recipient
	Text      string `protobuf:"bytes,2,opt,name=text,proto3" json:"text,omitempty"`           // Text to send to the recipient
}

func (x *ImRequest) Reset() {
	*x = ImRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_message_messaging_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ImRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ImRequest) ProtoMessage() {}

func (x *ImRequest) ProtoReflect() protoreflect.Message {
	mi := &file_message_messaging_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ImRequest.ProtoReflect.Descriptor instead.
func (*ImRequest) Descriptor() ([]byte, []int) {
	return file_message_messaging_proto_rawDescGZIP(), []int{2}
}

func (x *ImRequest) GetRecipient() string {
	if x != nil {
		return x.Recipient
	}
	return ""
}

func (x *ImRequest) GetText() string {
	if x != nil {
		return x.Text
	}
	return ""
}

type ImResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *ImResponse) Reset() {
	*x = ImResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_message_messaging_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ImResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ImResponse) ProtoMessage() {}

func (x *ImResponse) ProtoReflect() protoreflect.Message {
	mi := &file_message_messaging_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ImResponse.ProtoReflect.Descriptor instead.
func (*ImResponse) Descriptor() ([]byte, []int) {
	return file_message_messaging_proto_rawDescGZIP(), []int{3}
}

type LogRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Text string `protobuf:"bytes,1,opt,name=text,proto3" json:"text,omitempty"` // The log message to send
}

func (x *LogRequest) Reset() {
	*x = LogRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_message_messaging_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LogRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LogRequest) ProtoMessage() {}

func (x *LogRequest) ProtoReflect() protoreflect.Message {
	mi := &file_message_messaging_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LogRequest.ProtoReflect.Descriptor instead.
func (*LogRequest) Descriptor() ([]byte, []int) {
	return file_message_messaging_proto_rawDescGZIP(), []int{4}
}

func (x *LogRequest) GetText() string {
	if x != nil {
		return x.Text
	}
	return ""
}

type LogResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *LogResponse) Reset() {
	*x = LogResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_message_messaging_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LogResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LogResponse) ProtoMessage() {}

func (x *LogResponse) ProtoReflect() protoreflect.Message {
	mi := &file_message_messaging_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LogResponse.ProtoReflect.Descriptor instead.
func (*LogResponse) Descriptor() ([]byte, []int) {
	return file_message_messaging_proto_rawDescGZIP(), []int{5}
}

var File_message_messaging_proto protoreflect.FileDescriptor

var file_message_messaging_proto_rawDesc = []byte{
	0x0a, 0x17, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x69, 0x6e, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x09, 0x6d, 0x65, 0x73, 0x73, 0x61,
	0x67, 0x69, 0x6e, 0x67, 0x22, 0x9c, 0x01, 0x0a, 0x0b, 0x4d, 0x61, 0x69, 0x6c, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x66, 0x72, 0x6f, 0x6d, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x66, 0x72, 0x6f, 0x6d, 0x12, 0x19, 0x0a, 0x08, 0x72, 0x65, 0x70, 0x6c,
	0x79, 0x5f, 0x74, 0x6f, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x72, 0x65, 0x70, 0x6c,
	0x79, 0x54, 0x6f, 0x12, 0x0e, 0x0a, 0x02, 0x74, 0x6f, 0x18, 0x03, 0x20, 0x03, 0x28, 0x09, 0x52,
	0x02, 0x74, 0x6f, 0x12, 0x0e, 0x0a, 0x02, 0x63, 0x63, 0x18, 0x04, 0x20, 0x03, 0x28, 0x09, 0x52,
	0x02, 0x63, 0x63, 0x12, 0x10, 0x0a, 0x03, 0x62, 0x63, 0x63, 0x18, 0x05, 0x20, 0x03, 0x28, 0x09,
	0x52, 0x03, 0x62, 0x63, 0x63, 0x12, 0x18, 0x0a, 0x07, 0x73, 0x75, 0x62, 0x6a, 0x65, 0x63, 0x74,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x75, 0x62, 0x6a, 0x65, 0x63, 0x74, 0x12,
	0x12, 0x0a, 0x04, 0x62, 0x6f, 0x64, 0x79, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x62,
	0x6f, 0x64, 0x79, 0x22, 0x0e, 0x0a, 0x0c, 0x4d, 0x61, 0x69, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x22, 0x3d, 0x0a, 0x09, 0x49, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x1c, 0x0a, 0x09, 0x72, 0x65, 0x63, 0x69, 0x70, 0x69, 0x65, 0x6e, 0x74, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x72, 0x65, 0x63, 0x69, 0x70, 0x69, 0x65, 0x6e, 0x74, 0x12, 0x12,
	0x0a, 0x04, 0x74, 0x65, 0x78, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x65,
	0x78, 0x74, 0x22, 0x0c, 0x0a, 0x0a, 0x49, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x20, 0x0a, 0x0a, 0x4c, 0x6f, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12,
	0x0a, 0x04, 0x74, 0x65, 0x78, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x65,
	0x78, 0x74, 0x22, 0x0d, 0x0a, 0x0b, 0x4c, 0x6f, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x32, 0xfb, 0x02, 0x0a, 0x09, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x12,
	0x3b, 0x0a, 0x08, 0x53, 0x65, 0x6e, 0x64, 0x4d, 0x61, 0x69, 0x6c, 0x12, 0x16, 0x2e, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e, 0x4d, 0x61, 0x69, 0x6c, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x17, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e,
	0x4d, 0x61, 0x69, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3d, 0x0a, 0x0e,
	0x53, 0x65, 0x6e, 0x64, 0x49, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x49, 0x4d, 0x12, 0x14,
	0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e, 0x49, 0x6d, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67,
	0x2e, 0x49, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3d, 0x0a, 0x0e, 0x53,
	0x65, 0x6e, 0x64, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x49, 0x4d, 0x12, 0x14, 0x2e,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e, 0x49, 0x6d, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e,
	0x49, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x39, 0x0a, 0x08, 0x53, 0x65,
	0x6e, 0x64, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x15, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69,
	0x6e, 0x67, 0x2e, 0x4c, 0x6f, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e, 0x4c, 0x6f, 0x67, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3c, 0x0a, 0x0b, 0x53, 0x65, 0x6e, 0x64, 0x57, 0x61, 0x72,
	0x6e, 0x69, 0x6e, 0x67, 0x12, 0x15, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67,
	0x2e, 0x4c, 0x6f, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e, 0x4c, 0x6f, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x3a, 0x0a, 0x09, 0x53, 0x65, 0x6e, 0x64, 0x45, 0x72, 0x72, 0x6f, 0x72,
	0x12, 0x15, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x2e, 0x4c, 0x6f, 0x67,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x69, 0x6e, 0x67, 0x2e, 0x4c, 0x6f, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42,
	0x27, 0x5a, 0x25, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x65, 0x74, 0x68, 0x7a, 0x2e, 0x63,
	0x68, 0x2f, 0x6c, 0x6d, 0x6f, 0x65, 0x6c, 0x6c, 0x65, 0x72, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x6d,
	0x65, 0x73, 0x73, 0x61, 0x67, 0x69, 0x6e, 0x67, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_message_messaging_proto_rawDescOnce sync.Once
	file_message_messaging_proto_rawDescData = file_message_messaging_proto_rawDesc
)

func file_message_messaging_proto_rawDescGZIP() []byte {
	file_message_messaging_proto_rawDescOnce.Do(func() {
		file_message_messaging_proto_rawDescData = protoimpl.X.CompressGZIP(file_message_messaging_proto_rawDescData)
	})
	return file_message_messaging_proto_rawDescData
}

var file_message_messaging_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_message_messaging_proto_goTypes = []interface{}{
	(*MailRequest)(nil),  // 0: messaging.MailRequest
	(*MailResponse)(nil), // 1: messaging.MailResponse
	(*ImRequest)(nil),    // 2: messaging.ImRequest
	(*ImResponse)(nil),   // 3: messaging.ImResponse
	(*LogRequest)(nil),   // 4: messaging.LogRequest
	(*LogResponse)(nil),  // 5: messaging.LogResponse
}
var file_message_messaging_proto_depIdxs = []int32{
	0, // 0: messaging.Messaging.SendMail:input_type -> messaging.MailRequest
	2, // 1: messaging.Messaging.SendInternalIM:input_type -> messaging.ImRequest
	2, // 2: messaging.Messaging.SendExternalIM:input_type -> messaging.ImRequest
	4, // 3: messaging.Messaging.SendInfo:input_type -> messaging.LogRequest
	4, // 4: messaging.Messaging.SendWarning:input_type -> messaging.LogRequest
	4, // 5: messaging.Messaging.SendError:input_type -> messaging.LogRequest
	1, // 6: messaging.Messaging.SendMail:output_type -> messaging.MailResponse
	3, // 7: messaging.Messaging.SendInternalIM:output_type -> messaging.ImResponse
	3, // 8: messaging.Messaging.SendExternalIM:output_type -> messaging.ImResponse
	5, // 9: messaging.Messaging.SendInfo:output_type -> messaging.LogResponse
	5, // 10: messaging.Messaging.SendWarning:output_type -> messaging.LogResponse
	5, // 11: messaging.Messaging.SendError:output_type -> messaging.LogResponse
	6, // [6:12] is the sub-list for method output_type
	0, // [0:6] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_message_messaging_proto_init() }
func file_message_messaging_proto_init() {
	if File_message_messaging_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_message_messaging_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MailRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_message_messaging_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MailResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_message_messaging_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ImRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_message_messaging_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ImResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_message_messaging_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LogRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_message_messaging_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LogResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_message_messaging_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_message_messaging_proto_goTypes,
		DependencyIndexes: file_message_messaging_proto_depIdxs,
		MessageInfos:      file_message_messaging_proto_msgTypes,
	}.Build()
	File_message_messaging_proto = out.File
	file_message_messaging_proto_rawDesc = nil
	file_message_messaging_proto_goTypes = nil
	file_message_messaging_proto_depIdxs = nil
}
