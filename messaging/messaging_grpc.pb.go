// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package messaging

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// MessagingClient is the client API for Messaging service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MessagingClient interface {
	// Send a message via Mail - this needs privileged access
	SendMail(ctx context.Context, in *MailRequest, opts ...grpc.CallOption) (*MailResponse, error)
	// Send a message to the internal VIS Chat system
	SendInternalIM(ctx context.Context, in *ImRequest, opts ...grpc.CallOption) (*ImResponse, error)
	// Send a message to the external VIS Chat system
	SendExternalIM(ctx context.Context, in *ImRequest, opts ...grpc.CallOption) (*ImResponse, error)
	// Send an info message to the VIS Chat System
	// This endpoint should be used for logging information that boardmembers should know
	SendInfo(ctx context.Context, in *LogRequest, opts ...grpc.CallOption) (*LogResponse, error)
	// Send a warning message to the VIS Chat System
	// This endpoint should be used for logging information that boardmembers should know
	SendWarning(ctx context.Context, in *LogRequest, opts ...grpc.CallOption) (*LogResponse, error)
	// Send a error message to the VIS Chat System
	// This endpoint should be used for logging information that boardmembers should
	// really urgently know about - it will even send a mail in case of failed IM
	SendError(ctx context.Context, in *LogRequest, opts ...grpc.CallOption) (*LogResponse, error)
}

type messagingClient struct {
	cc grpc.ClientConnInterface
}

func NewMessagingClient(cc grpc.ClientConnInterface) MessagingClient {
	return &messagingClient{cc}
}

func (c *messagingClient) SendMail(ctx context.Context, in *MailRequest, opts ...grpc.CallOption) (*MailResponse, error) {
	out := new(MailResponse)
	err := c.cc.Invoke(ctx, "/messaging.Messaging/SendMail", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagingClient) SendInternalIM(ctx context.Context, in *ImRequest, opts ...grpc.CallOption) (*ImResponse, error) {
	out := new(ImResponse)
	err := c.cc.Invoke(ctx, "/messaging.Messaging/SendInternalIM", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagingClient) SendExternalIM(ctx context.Context, in *ImRequest, opts ...grpc.CallOption) (*ImResponse, error) {
	out := new(ImResponse)
	err := c.cc.Invoke(ctx, "/messaging.Messaging/SendExternalIM", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagingClient) SendInfo(ctx context.Context, in *LogRequest, opts ...grpc.CallOption) (*LogResponse, error) {
	out := new(LogResponse)
	err := c.cc.Invoke(ctx, "/messaging.Messaging/SendInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagingClient) SendWarning(ctx context.Context, in *LogRequest, opts ...grpc.CallOption) (*LogResponse, error) {
	out := new(LogResponse)
	err := c.cc.Invoke(ctx, "/messaging.Messaging/SendWarning", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagingClient) SendError(ctx context.Context, in *LogRequest, opts ...grpc.CallOption) (*LogResponse, error) {
	out := new(LogResponse)
	err := c.cc.Invoke(ctx, "/messaging.Messaging/SendError", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MessagingServer is the server API for Messaging service.
// All implementations must embed UnimplementedMessagingServer
// for forward compatibility
type MessagingServer interface {
	// Send a message via Mail - this needs privileged access
	SendMail(context.Context, *MailRequest) (*MailResponse, error)
	// Send a message to the internal VIS Chat system
	SendInternalIM(context.Context, *ImRequest) (*ImResponse, error)
	// Send a message to the external VIS Chat system
	SendExternalIM(context.Context, *ImRequest) (*ImResponse, error)
	// Send an info message to the VIS Chat System
	// This endpoint should be used for logging information that boardmembers should know
	SendInfo(context.Context, *LogRequest) (*LogResponse, error)
	// Send a warning message to the VIS Chat System
	// This endpoint should be used for logging information that boardmembers should know
	SendWarning(context.Context, *LogRequest) (*LogResponse, error)
	// Send a error message to the VIS Chat System
	// This endpoint should be used for logging information that boardmembers should
	// really urgently know about - it will even send a mail in case of failed IM
	SendError(context.Context, *LogRequest) (*LogResponse, error)
	mustEmbedUnimplementedMessagingServer()
}

// UnimplementedMessagingServer must be embedded to have forward compatible implementations.
type UnimplementedMessagingServer struct {
}

func (UnimplementedMessagingServer) SendMail(context.Context, *MailRequest) (*MailResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendMail not implemented")
}
func (UnimplementedMessagingServer) SendInternalIM(context.Context, *ImRequest) (*ImResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendInternalIM not implemented")
}
func (UnimplementedMessagingServer) SendExternalIM(context.Context, *ImRequest) (*ImResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendExternalIM not implemented")
}
func (UnimplementedMessagingServer) SendInfo(context.Context, *LogRequest) (*LogResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendInfo not implemented")
}
func (UnimplementedMessagingServer) SendWarning(context.Context, *LogRequest) (*LogResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendWarning not implemented")
}
func (UnimplementedMessagingServer) SendError(context.Context, *LogRequest) (*LogResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendError not implemented")
}
func (UnimplementedMessagingServer) mustEmbedUnimplementedMessagingServer() {}

// UnsafeMessagingServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MessagingServer will
// result in compilation errors.
type UnsafeMessagingServer interface {
	mustEmbedUnimplementedMessagingServer()
}

func RegisterMessagingServer(s grpc.ServiceRegistrar, srv MessagingServer) {
	s.RegisterService(&_Messaging_serviceDesc, srv)
}

func _Messaging_SendMail_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MailRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagingServer).SendMail(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/messaging.Messaging/SendMail",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagingServer).SendMail(ctx, req.(*MailRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Messaging_SendInternalIM_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ImRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagingServer).SendInternalIM(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/messaging.Messaging/SendInternalIM",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagingServer).SendInternalIM(ctx, req.(*ImRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Messaging_SendExternalIM_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ImRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagingServer).SendExternalIM(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/messaging.Messaging/SendExternalIM",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagingServer).SendExternalIM(ctx, req.(*ImRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Messaging_SendInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LogRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagingServer).SendInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/messaging.Messaging/SendInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagingServer).SendInfo(ctx, req.(*LogRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Messaging_SendWarning_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LogRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagingServer).SendWarning(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/messaging.Messaging/SendWarning",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagingServer).SendWarning(ctx, req.(*LogRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Messaging_SendError_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LogRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagingServer).SendError(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/messaging.Messaging/SendError",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagingServer).SendError(ctx, req.(*LogRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Messaging_serviceDesc = grpc.ServiceDesc{
	ServiceName: "messaging.Messaging",
	HandlerType: (*MessagingServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SendMail",
			Handler:    _Messaging_SendMail_Handler,
		},
		{
			MethodName: "SendInternalIM",
			Handler:    _Messaging_SendInternalIM_Handler,
		},
		{
			MethodName: "SendExternalIM",
			Handler:    _Messaging_SendExternalIM_Handler,
		},
		{
			MethodName: "SendInfo",
			Handler:    _Messaging_SendInfo_Handler,
		},
		{
			MethodName: "SendWarning",
			Handler:    _Messaging_SendWarning_Handler,
		},
		{
			MethodName: "SendError",
			Handler:    _Messaging_SendError_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "message/messaging.proto",
}
