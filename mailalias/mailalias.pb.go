// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.11.4
// source: mailalias/mailalias.proto

package mailalias

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type SystemStatusResponse_SyncStatus int32

const (
	SystemStatusResponse_UNKNOWN    SystemStatusResponse_SyncStatus = 0 // The API has no connection to this server. It is unknown which aliases it has.
	SystemStatusResponse_UP_TO_DATE SystemStatusResponse_SyncStatus = 1 // The server is connected and has up to date aliases.
	SystemStatusResponse_OUTDATED   SystemStatusResponse_SyncStatus = 2 // The server is connected, but doesn't have up to date aliases. This is typically transient.
)

// Enum value maps for SystemStatusResponse_SyncStatus.
var (
	SystemStatusResponse_SyncStatus_name = map[int32]string{
		0: "UNKNOWN",
		1: "UP_TO_DATE",
		2: "OUTDATED",
	}
	SystemStatusResponse_SyncStatus_value = map[string]int32{
		"UNKNOWN":    0,
		"UP_TO_DATE": 1,
		"OUTDATED":   2,
	}
)

func (x SystemStatusResponse_SyncStatus) Enum() *SystemStatusResponse_SyncStatus {
	p := new(SystemStatusResponse_SyncStatus)
	*p = x
	return p
}

func (x SystemStatusResponse_SyncStatus) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SystemStatusResponse_SyncStatus) Descriptor() protoreflect.EnumDescriptor {
	return file_mailalias_mailalias_proto_enumTypes[0].Descriptor()
}

func (SystemStatusResponse_SyncStatus) Type() protoreflect.EnumType {
	return &file_mailalias_mailalias_proto_enumTypes[0]
}

func (x SystemStatusResponse_SyncStatus) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SystemStatusResponse_SyncStatus.Descriptor instead.
func (SystemStatusResponse_SyncStatus) EnumDescriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{8, 0}
}

type Empty struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *Empty) Reset() {
	*x = Empty{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Empty) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Empty) ProtoMessage() {}

func (x *Empty) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Empty.ProtoReflect.Descriptor instead.
func (*Empty) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{0}
}

type Alias struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Email going to the "source" address is sent to the "destination" address.
	// This should be a VIS-controlled address, for example "vorstand@vis.ethz.ch"
	Source string `protobuf:"bytes,1,opt,name=source,proto3" json:"source,omitempty"`
	// The "destination" address receives all email which was sent to the "source" address.
	// This may or may not be a VIS-controlled address, for example "jdoe@vis.ethz.ch" or "jdoe@example.com".
	Destination string `protobuf:"bytes,2,opt,name=destination,proto3" json:"destination,omitempty"` // email
	// Operations on locked aliases require a priviliged API key.
	Locked bool `protobuf:"varint,3,opt,name=locked,proto3" json:"locked,omitempty"`
	// If "can_send" is true, it is possible to send emails with
	// the "source" address in the sender field.
	CanSend bool `protobuf:"varint,4,opt,name=can_send,json=canSend,proto3" json:"can_send,omitempty"`
	// A human readable comment about this alias. Can be empty.
	Comment string `protobuf:"bytes,5,opt,name=comment,proto3" json:"comment,omitempty"`
	// User, who created the alias. May be empty if user cannot be determined.
	CreatedBy string `protobuf:"bytes,6,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	// Unix-timestamp (seconds) of the time of creation of the alias
	CreatedAt int64 `protobuf:"varint,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
}

func (x *Alias) Reset() {
	*x = Alias{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Alias) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Alias) ProtoMessage() {}

func (x *Alias) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Alias.ProtoReflect.Descriptor instead.
func (*Alias) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{1}
}

func (x *Alias) GetSource() string {
	if x != nil {
		return x.Source
	}
	return ""
}

func (x *Alias) GetDestination() string {
	if x != nil {
		return x.Destination
	}
	return ""
}

func (x *Alias) GetLocked() bool {
	if x != nil {
		return x.Locked
	}
	return false
}

func (x *Alias) GetCanSend() bool {
	if x != nil {
		return x.CanSend
	}
	return false
}

func (x *Alias) GetComment() string {
	if x != nil {
		return x.Comment
	}
	return ""
}

func (x *Alias) GetCreatedBy() string {
	if x != nil {
		return x.CreatedBy
	}
	return ""
}

func (x *Alias) GetCreatedAt() int64 {
	if x != nil {
		return x.CreatedAt
	}
	return 0
}

type ListAliasesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// email, optional
	// If supplied, only aliases with that "source" are returned.
	Source string `protobuf:"bytes,1,opt,name=source,proto3" json:"source,omitempty"`
	// email, optional
	// If supplied, only aliases with that "destination" are returned.
	Destination string `protobuf:"bytes,2,opt,name=destination,proto3" json:"destination,omitempty"`
}

func (x *ListAliasesRequest) Reset() {
	*x = ListAliasesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListAliasesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListAliasesRequest) ProtoMessage() {}

func (x *ListAliasesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListAliasesRequest.ProtoReflect.Descriptor instead.
func (*ListAliasesRequest) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{2}
}

func (x *ListAliasesRequest) GetSource() string {
	if x != nil {
		return x.Source
	}
	return ""
}

func (x *ListAliasesRequest) GetDestination() string {
	if x != nil {
		return x.Destination
	}
	return ""
}

type CreateAliasRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Source      string `protobuf:"bytes,1,opt,name=source,proto3" json:"source,omitempty"`           // email, required
	Destination string `protobuf:"bytes,2,opt,name=destination,proto3" json:"destination,omitempty"` // email, required
	Locked      bool   `protobuf:"varint,3,opt,name=locked,proto3" json:"locked,omitempty"`
	CanSend     bool   `protobuf:"varint,4,opt,name=can_send,json=canSend,proto3" json:"can_send,omitempty"`
	Comment     string `protobuf:"bytes,5,opt,name=comment,proto3" json:"comment,omitempty"` // optional
}

func (x *CreateAliasRequest) Reset() {
	*x = CreateAliasRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateAliasRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateAliasRequest) ProtoMessage() {}

func (x *CreateAliasRequest) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateAliasRequest.ProtoReflect.Descriptor instead.
func (*CreateAliasRequest) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{3}
}

func (x *CreateAliasRequest) GetSource() string {
	if x != nil {
		return x.Source
	}
	return ""
}

func (x *CreateAliasRequest) GetDestination() string {
	if x != nil {
		return x.Destination
	}
	return ""
}

func (x *CreateAliasRequest) GetLocked() bool {
	if x != nil {
		return x.Locked
	}
	return false
}

func (x *CreateAliasRequest) GetCanSend() bool {
	if x != nil {
		return x.CanSend
	}
	return false
}

func (x *CreateAliasRequest) GetComment() string {
	if x != nil {
		return x.Comment
	}
	return ""
}

type DeleteAliasRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Source      string `protobuf:"bytes,1,opt,name=source,proto3" json:"source,omitempty"`           // email, required
	Destination string `protobuf:"bytes,2,opt,name=destination,proto3" json:"destination,omitempty"` // email, required
}

func (x *DeleteAliasRequest) Reset() {
	*x = DeleteAliasRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteAliasRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteAliasRequest) ProtoMessage() {}

func (x *DeleteAliasRequest) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteAliasRequest.ProtoReflect.Descriptor instead.
func (*DeleteAliasRequest) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{4}
}

func (x *DeleteAliasRequest) GetSource() string {
	if x != nil {
		return x.Source
	}
	return ""
}

func (x *DeleteAliasRequest) GetDestination() string {
	if x != nil {
		return x.Destination
	}
	return ""
}

type UserLoginRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Username string `protobuf:"bytes,1,opt,name=username,proto3" json:"username,omitempty"` // required
	Password string `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"` // required
}

func (x *UserLoginRequest) Reset() {
	*x = UserLoginRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UserLoginRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UserLoginRequest) ProtoMessage() {}

func (x *UserLoginRequest) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UserLoginRequest.ProtoReflect.Descriptor instead.
func (*UserLoginRequest) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{5}
}

func (x *UserLoginRequest) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

func (x *UserLoginRequest) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

type UserLoginResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// If the credentials are wrong, a PermissionDenied (7) error is returned.
	SessionToken string `protobuf:"bytes,1,opt,name=session_token,json=sessionToken,proto3" json:"session_token,omitempty"`
}

func (x *UserLoginResponse) Reset() {
	*x = UserLoginResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UserLoginResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UserLoginResponse) ProtoMessage() {}

func (x *UserLoginResponse) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UserLoginResponse.ProtoReflect.Descriptor instead.
func (*UserLoginResponse) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{6}
}

func (x *UserLoginResponse) GetSessionToken() string {
	if x != nil {
		return x.SessionToken
	}
	return ""
}

type LoginValidationRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SessionToken string `protobuf:"bytes,1,opt,name=session_token,json=sessionToken,proto3" json:"session_token,omitempty"` // required
}

func (x *LoginValidationRequest) Reset() {
	*x = LoginValidationRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LoginValidationRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LoginValidationRequest) ProtoMessage() {}

func (x *LoginValidationRequest) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LoginValidationRequest.ProtoReflect.Descriptor instead.
func (*LoginValidationRequest) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{7}
}

func (x *LoginValidationRequest) GetSessionToken() string {
	if x != nil {
		return x.SessionToken
	}
	return ""
}

type SystemStatusResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	MailServers   []*SystemStatusResponse_Server `protobuf:"bytes,1,rep,name=mail_servers,json=mailServers,proto3" json:"mail_servers,omitempty"`
	ShadowedExist bool                           `protobuf:"varint,2,opt,name=shadowed_exist,json=shadowedExist,proto3" json:"shadowed_exist,omitempty"` // Some aliases are shadowed by others. Administrators should have already been notified.
}

func (x *SystemStatusResponse) Reset() {
	*x = SystemStatusResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SystemStatusResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SystemStatusResponse) ProtoMessage() {}

func (x *SystemStatusResponse) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SystemStatusResponse.ProtoReflect.Descriptor instead.
func (*SystemStatusResponse) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{8}
}

func (x *SystemStatusResponse) GetMailServers() []*SystemStatusResponse_Server {
	if x != nil {
		return x.MailServers
	}
	return nil
}

func (x *SystemStatusResponse) GetShadowedExist() bool {
	if x != nil {
		return x.ShadowedExist
	}
	return false
}

type SystemStatusResponse_Server struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name   string                          `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Status SystemStatusResponse_SyncStatus `protobuf:"varint,1,opt,name=status,proto3,enum=mailalias.SystemStatusResponse_SyncStatus" json:"status,omitempty"`
}

func (x *SystemStatusResponse_Server) Reset() {
	*x = SystemStatusResponse_Server{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailalias_mailalias_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SystemStatusResponse_Server) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SystemStatusResponse_Server) ProtoMessage() {}

func (x *SystemStatusResponse_Server) ProtoReflect() protoreflect.Message {
	mi := &file_mailalias_mailalias_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SystemStatusResponse_Server.ProtoReflect.Descriptor instead.
func (*SystemStatusResponse_Server) Descriptor() ([]byte, []int) {
	return file_mailalias_mailalias_proto_rawDescGZIP(), []int{8, 0}
}

func (x *SystemStatusResponse_Server) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *SystemStatusResponse_Server) GetStatus() SystemStatusResponse_SyncStatus {
	if x != nil {
		return x.Status
	}
	return SystemStatusResponse_UNKNOWN
}

var File_mailalias_mailalias_proto protoreflect.FileDescriptor

var file_mailalias_mailalias_proto_rawDesc = []byte{
	0x0a, 0x19, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73, 0x2f, 0x6d, 0x61, 0x69, 0x6c,
	0x61, 0x6c, 0x69, 0x61, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x09, 0x6d, 0x61, 0x69,
	0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73, 0x22, 0x07, 0x0a, 0x05, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0xcc, 0x01, 0x0a, 0x05, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x6f, 0x75,
	0x72, 0x63, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63,
	0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x12, 0x16, 0x0a, 0x06, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x08, 0x52, 0x06, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x63,
	0x61, 0x6e, 0x5f, 0x73, 0x65, 0x6e, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x63,
	0x61, 0x6e, 0x53, 0x65, 0x6e, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x63, 0x6f, 0x6d, 0x6d, 0x65, 0x6e,
	0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x63, 0x6f, 0x6d, 0x6d, 0x65, 0x6e, 0x74,
	0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x62, 0x79, 0x18, 0x06,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x42, 0x79, 0x12,
	0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x4e,
	0x0a, 0x12, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x65, 0x73, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x12, 0x20, 0x0a, 0x0b,
	0x64, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x9b,
	0x01, 0x0a, 0x12, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x12, 0x20, 0x0a,
	0x0b, 0x64, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x16, 0x0a, 0x06, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x08, 0x52,
	0x06, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x63, 0x61, 0x6e, 0x5f, 0x73,
	0x65, 0x6e, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x63, 0x61, 0x6e, 0x53, 0x65,
	0x6e, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x63, 0x6f, 0x6d, 0x6d, 0x65, 0x6e, 0x74, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x63, 0x6f, 0x6d, 0x6d, 0x65, 0x6e, 0x74, 0x22, 0x4e, 0x0a, 0x12,
	0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65,
	0x73, 0x74, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x64, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x4a, 0x0a, 0x10,
	0x55, 0x73, 0x65, 0x72, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08,
	0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x22, 0x38, 0x0a, 0x11, 0x55, 0x73, 0x65, 0x72,
	0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x23, 0x0a,
	0x0d, 0x73, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x73, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x54, 0x6f, 0x6b,
	0x65, 0x6e, 0x22, 0x3d, 0x0a, 0x16, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x56, 0x61, 0x6c, 0x69, 0x64,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x23, 0x0a, 0x0d,
	0x73, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0c, 0x73, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x54, 0x6f, 0x6b, 0x65,
	0x6e, 0x22, 0xa3, 0x02, 0x0a, 0x14, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x53, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x49, 0x0a, 0x0c, 0x6d, 0x61,
	0x69, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x26, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73, 0x2e, 0x53, 0x79, 0x73,
	0x74, 0x65, 0x6d, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x52, 0x0b, 0x6d, 0x61, 0x69, 0x6c, 0x53, 0x65,
	0x72, 0x76, 0x65, 0x72, 0x73, 0x12, 0x25, 0x0a, 0x0e, 0x73, 0x68, 0x61, 0x64, 0x6f, 0x77, 0x65,
	0x64, 0x5f, 0x65, 0x78, 0x69, 0x73, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0d, 0x73,
	0x68, 0x61, 0x64, 0x6f, 0x77, 0x65, 0x64, 0x45, 0x78, 0x69, 0x73, 0x74, 0x1a, 0x60, 0x0a, 0x06,
	0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x42, 0x0a, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x2a, 0x2e, 0x6d, 0x61, 0x69,
	0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73, 0x2e, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x53, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x2e, 0x53, 0x79, 0x6e, 0x63,
	0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x37,
	0x0a, 0x0a, 0x53, 0x79, 0x6e, 0x63, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x0b, 0x0a, 0x07,
	0x55, 0x4e, 0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x0e, 0x0a, 0x0a, 0x55, 0x50, 0x5f,
	0x54, 0x4f, 0x5f, 0x44, 0x41, 0x54, 0x45, 0x10, 0x01, 0x12, 0x0c, 0x0a, 0x08, 0x4f, 0x55, 0x54,
	0x44, 0x41, 0x54, 0x45, 0x44, 0x10, 0x02, 0x32, 0xa0, 0x03, 0x0a, 0x09, 0x4d, 0x61, 0x69, 0x6c,
	0x61, 0x6c, 0x69, 0x61, 0x73, 0x12, 0x40, 0x0a, 0x0b, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x6c, 0x69,
	0x61, 0x73, 0x65, 0x73, 0x12, 0x1d, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73,
	0x2e, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x10, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73, 0x2e,
	0x41, 0x6c, 0x69, 0x61, 0x73, 0x30, 0x01, 0x12, 0x3e, 0x0a, 0x0b, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x12, 0x1d, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69,
	0x61, 0x73, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x10, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61,
	0x73, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x12, 0x3e, 0x0a, 0x0b, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x12, 0x1d, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69,
	0x61, 0x73, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x41, 0x6c, 0x69, 0x61, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x10, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61,
	0x73, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x12, 0x46, 0x0a, 0x09, 0x55, 0x73, 0x65, 0x72, 0x4c,
	0x6f, 0x67, 0x69, 0x6e, 0x12, 0x1b, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73,
	0x2e, 0x55, 0x73, 0x65, 0x72, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x1c, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73, 0x2e, 0x55, 0x73,
	0x65, 0x72, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x46, 0x0a, 0x0f, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x56, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x12, 0x21, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61, 0x73, 0x2e, 0x4c,
	0x6f, 0x67, 0x69, 0x6e, 0x56, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x10, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c, 0x69, 0x61,
	0x73, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x12, 0x41, 0x0a, 0x0c, 0x53, 0x79, 0x73, 0x74, 0x65,
	0x6d, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x10, 0x2e, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c,
	0x69, 0x61, 0x73, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x1f, 0x2e, 0x6d, 0x61, 0x69, 0x6c,
	0x61, 0x6c, 0x69, 0x61, 0x73, 0x2e, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x53, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x27, 0x5a, 0x25, 0x67, 0x69,
	0x74, 0x6c, 0x61, 0x62, 0x2e, 0x65, 0x74, 0x68, 0x7a, 0x2e, 0x63, 0x68, 0x2f, 0x6c, 0x6d, 0x6f,
	0x65, 0x6c, 0x6c, 0x65, 0x72, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x6d, 0x61, 0x69, 0x6c, 0x61, 0x6c,
	0x69, 0x61, 0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_mailalias_mailalias_proto_rawDescOnce sync.Once
	file_mailalias_mailalias_proto_rawDescData = file_mailalias_mailalias_proto_rawDesc
)

func file_mailalias_mailalias_proto_rawDescGZIP() []byte {
	file_mailalias_mailalias_proto_rawDescOnce.Do(func() {
		file_mailalias_mailalias_proto_rawDescData = protoimpl.X.CompressGZIP(file_mailalias_mailalias_proto_rawDescData)
	})
	return file_mailalias_mailalias_proto_rawDescData
}

var file_mailalias_mailalias_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_mailalias_mailalias_proto_msgTypes = make([]protoimpl.MessageInfo, 10)
var file_mailalias_mailalias_proto_goTypes = []interface{}{
	(SystemStatusResponse_SyncStatus)(0), // 0: mailalias.SystemStatusResponse.SyncStatus
	(*Empty)(nil),                        // 1: mailalias.Empty
	(*Alias)(nil),                        // 2: mailalias.Alias
	(*ListAliasesRequest)(nil),           // 3: mailalias.ListAliasesRequest
	(*CreateAliasRequest)(nil),           // 4: mailalias.CreateAliasRequest
	(*DeleteAliasRequest)(nil),           // 5: mailalias.DeleteAliasRequest
	(*UserLoginRequest)(nil),             // 6: mailalias.UserLoginRequest
	(*UserLoginResponse)(nil),            // 7: mailalias.UserLoginResponse
	(*LoginValidationRequest)(nil),       // 8: mailalias.LoginValidationRequest
	(*SystemStatusResponse)(nil),         // 9: mailalias.SystemStatusResponse
	(*SystemStatusResponse_Server)(nil),  // 10: mailalias.SystemStatusResponse.Server
}
var file_mailalias_mailalias_proto_depIdxs = []int32{
	10, // 0: mailalias.SystemStatusResponse.mail_servers:type_name -> mailalias.SystemStatusResponse.Server
	0,  // 1: mailalias.SystemStatusResponse.Server.status:type_name -> mailalias.SystemStatusResponse.SyncStatus
	3,  // 2: mailalias.Mailalias.ListAliases:input_type -> mailalias.ListAliasesRequest
	4,  // 3: mailalias.Mailalias.CreateAlias:input_type -> mailalias.CreateAliasRequest
	5,  // 4: mailalias.Mailalias.DeleteAlias:input_type -> mailalias.DeleteAliasRequest
	6,  // 5: mailalias.Mailalias.UserLogin:input_type -> mailalias.UserLoginRequest
	8,  // 6: mailalias.Mailalias.LoginValidation:input_type -> mailalias.LoginValidationRequest
	1,  // 7: mailalias.Mailalias.SystemStatus:input_type -> mailalias.Empty
	2,  // 8: mailalias.Mailalias.ListAliases:output_type -> mailalias.Alias
	1,  // 9: mailalias.Mailalias.CreateAlias:output_type -> mailalias.Empty
	1,  // 10: mailalias.Mailalias.DeleteAlias:output_type -> mailalias.Empty
	7,  // 11: mailalias.Mailalias.UserLogin:output_type -> mailalias.UserLoginResponse
	1,  // 12: mailalias.Mailalias.LoginValidation:output_type -> mailalias.Empty
	9,  // 13: mailalias.Mailalias.SystemStatus:output_type -> mailalias.SystemStatusResponse
	8,  // [8:14] is the sub-list for method output_type
	2,  // [2:8] is the sub-list for method input_type
	2,  // [2:2] is the sub-list for extension type_name
	2,  // [2:2] is the sub-list for extension extendee
	0,  // [0:2] is the sub-list for field type_name
}

func init() { file_mailalias_mailalias_proto_init() }
func file_mailalias_mailalias_proto_init() {
	if File_mailalias_mailalias_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_mailalias_mailalias_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Empty); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Alias); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListAliasesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateAliasRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteAliasRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UserLoginRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UserLoginResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LoginValidationRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SystemStatusResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailalias_mailalias_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SystemStatusResponse_Server); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_mailalias_mailalias_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   10,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_mailalias_mailalias_proto_goTypes,
		DependencyIndexes: file_mailalias_mailalias_proto_depIdxs,
		EnumInfos:         file_mailalias_mailalias_proto_enumTypes,
		MessageInfos:      file_mailalias_mailalias_proto_msgTypes,
	}.Build()
	File_mailalias_mailalias_proto = out.File
	file_mailalias_mailalias_proto_rawDesc = nil
	file_mailalias_mailalias_proto_goTypes = nil
	file_mailalias_mailalias_proto_depIdxs = nil
}
